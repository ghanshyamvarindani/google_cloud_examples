#################################################################
# Author : Ghanshyam Varindani
# Script to explain the Bigquery connectivity using Python
# Date: 18 December 2020
#################################################################

# package to create dataframe(Pandas), bigquery (Bigquery) and Service account authentication(oauth2)
import pandas as pd
from google.cloud import bigquery
from google.oauth2 import service_account

# Create connection with Google Cloud, json need to be created using IAM policy. Service Account need to be created with Bigquery Admin role. Json can be exported and stored at local"
credentials =service_account.Credentials.from_service_account_file("<SA_account.json>")

# define project name 
project_id="<xxx>"

# bigquery connection
client = bigquery.Client(credentials=credentials,project=project_id)

# sql query 
sql="SELECT * FROM <project>.<dataset>.<tablename>"

#loading data into dataframe
df=client.query(sql).to_dataframe()

#print top 5
print(df.head(5))