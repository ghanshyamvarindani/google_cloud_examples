#!/usr/bin/env python3

__author__ = "generic"
__version__ = "0.1.0"
__license__ = "m"

import argparse
import json


def read_json_file(file_path):
    print("Reading JSON file from {} ... ".format(file_path))
    try:
        with open(file_path, 'r') as infile:
            json_obj = json.load(infile)
            print("Done")
            return json_obj
    except IOError as e:
        print(e)


def main(_args):
    print("Kubernest Operator testing")
    print(" date = {}".format(_args.date))
    print("Test if sevice account JSONs were deployed correctly")
    print(read_json_file(_args.storage_sa))
    print(read_json_file(_args.bq_sa))
    print("Now, you can authenticate clients using service acconut")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--date", action="store", help="execution date as YYYY-MM-DD", required=True)
    parser.add_argument("--storage-sa", action="store", help="file path to storage service account", required=True)
    parser.add_argument("--bq-sa", action="store", help="flie path to biq query service account", required=True)

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()
    main(args)
